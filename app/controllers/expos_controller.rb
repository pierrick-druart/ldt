class ExposController < InheritedResources::Base
respond_to :xml, :json, :html

def index

      @expos = Expo.all

      respond_with(@expos)
    end

    def new
      @expo = Expo.new
      respond_with(@expo)
    end

    def create
      @expo = Expo.create(params[:expo])
      respond_with(@expo)
    end

    def edit
      @expo = Expo.find(params[:id])
      respond_with(@expo)
    end


    def show
      @expo = Expo.find(params[:id])
      respond_with(@expo)
    end

    def update
        @expo = Expo.find(params[:id])
        @expo.update_attributes(params[:expo])
        respond_with(@expo)
    end

    def destroy
        @expo = Expo.find(params[:id]).destroy
        respond_with(@expo)
    end

end
