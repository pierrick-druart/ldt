class FurnituresController < InheritedResources::Base
respond_to :xml, :json, :html

def index

      @furnitures = Furniture.all

      respond_with(@furnitures)
    end

    def new
      @furniture = Furniture.new
      respond_with(@furniture)
    end

    def create
      @furniture = Furniture.create(params[:furniture])
      respond_with(@furniture)
    end

    def edit
      @furniture = Furniture.find(params[:id])
      respond_with(@furniture)
    end


    def show
      @furniture = Furniture.find(params[:id])
      respond_with(@furniture)
    end

    def update
        @furniture = Furniture.find(params[:id])
        @furniture.update_attributes(params[:furniture])
        respond_with(@furniture)
    end

    def destroy
        @furniture = Furniture.find(params[:id]).destroy
        respond_with(@furniture)
    end
end
