class CreateExpos < ActiveRecord::Migration
  def change
    create_table :expos do |t|
      t.string :title
      t.text :description
      t.string :name_location
      t.string :address
      t.datetime :date
      t.string :photo

      t.timestamps
    end
  end
end
