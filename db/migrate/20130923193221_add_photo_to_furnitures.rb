class AddPhotoToFurnitures < ActiveRecord::Migration
  def self.up
    add_attachment :furnitures, :photo
  end

  def self.down
    remove_attachment :furnitures, :photo
  end
end
