class CreateFindmes < ActiveRecord::Migration
  def change
    create_table :findmes do |t|
      t.string :title
      t.text :description
      t.string :name_location
      t.string :address
      t.string :photo

      t.timestamps
    end
  end
end
