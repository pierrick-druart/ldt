class AddLongitudeToExpo < ActiveRecord::Migration
  def change
    add_column :expos, :longitude, :float
  end
end
