class CreateFurnitures < ActiveRecord::Migration
  def change
    create_table :furnitures do |t|
      t.string :title
      t.text :description
      t.string :order_type
      t.string :photo

      t.timestamps
    end
  end
end
