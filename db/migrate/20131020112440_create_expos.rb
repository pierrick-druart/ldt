class CreateExpos < ActiveRecord::Migration
  def change
    create_table :expos do |t|
      t.string :title
      t.text :description
      t.datetime :date_time
      t.string :address
      t.string :city

      t.timestamps
    end
  end
end
