class AddLatitudeToExpo < ActiveRecord::Migration
  def change
    add_column :expos, :latitude, :float
  end
end
