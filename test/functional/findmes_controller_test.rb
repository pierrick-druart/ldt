require 'test_helper'

class FindmesControllerTest < ActionController::TestCase
  setup do
    @findme = findmes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:findmes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create findme" do
    assert_difference('Findme.count') do
      post :create, findme: { address: @findme.address, description: @findme.description, name_location: @findme.name_location, photo: @findme.photo, title: @findme.title }
    end

    assert_redirected_to findme_path(assigns(:findme))
  end

  test "should show findme" do
    get :show, id: @findme
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @findme
    assert_response :success
  end

  test "should update findme" do
    put :update, id: @findme, findme: { address: @findme.address, description: @findme.description, name_location: @findme.name_location, photo: @findme.photo, title: @findme.title }
    assert_redirected_to findme_path(assigns(:findme))
  end

  test "should destroy findme" do
    assert_difference('Findme.count', -1) do
      delete :destroy, id: @findme
    end

    assert_redirected_to findmes_path
  end
end
